Установка сервиса
=========

Необходимые требования
-----------

Перед тем как установить сервис, необходима установить инфраструктуру. Ссылка на установку инфарструктуры:
- https://gitlab.com/waceem7/infra 


Установка instahelper 
------------

IP адреса
Добавить в файл hosts или изменить файл в каталоге /etc/ansible/hosts. В файле нужно указать ip адрес вашего сервера:
instahelper_1 ansible_host=3.238.221.38 ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

1. instahelper_1 - любое имя для сервера
2. ansible_host - ip адрес сервера на котором хотите установить nginx
3. ansible_user - пользователь под которым будет производится подключение
4. ansible_ssh_private_key - сертификат, с помощью которого возможно подключение к серверу (его нужно создать зарание)

Запуск сервиса
--------------

Чтобы запустить сервис, необходимо выполнить команду:
    ansible-playbook instahelper.yml -b -vv
- -b - запуск от имени администратора
- -vv - лог для отслеживание происходящих событий на сервере

